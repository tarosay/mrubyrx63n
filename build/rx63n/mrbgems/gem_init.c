/*
 * This file contains a list of all
 * initializing methods which are
 * necessary to bootstrap all gems.
 *
 * IMPORTANT:
 *   This file was generated!
 *   All manual changes will get lost.
 */

#include "mruby.h"

void GENERATED_TMP_mrb_mruby_numeric_ext_gem_init(mrb_state* mrb);

void GENERATED_TMP_mrb_mruby_numeric_ext_gem_final(mrb_state* mrb);

static void
mrb_final_mrbgems(mrb_state *mrb) {
GENERATED_TMP_mrb_mruby_numeric_ext_gem_final(mrb);
}

void
mrb_init_mrbgems(mrb_state *mrb) {
GENERATED_TMP_mrb_mruby_numeric_ext_gem_init(mrb);
mrb_state_atexit(mrb, mrb_final_mrbgems);
}
